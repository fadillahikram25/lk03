class Pertambahan extends Kalkulator {
    public void setOperan(double operandA, double operandB){
        this.operand1 = operandA;
        this.operand2 = operandB;
    }
    public double hitung(){
        return operand1 + operand2;
    }

}